FROM python:3.10.10-alpine3.16

RUN apk update && apk upgrade
RUN apk add curl

WORKDIR /food-camp

COPY requirements.txt requirements.txt
RUN pip3 install --upgrade pip && pip3 install -r requirements.txt && pip3 install flask[async]

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]