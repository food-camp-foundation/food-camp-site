use food_camp;

CREATE TABLE food (
    id SERIAL PRIMARY KEY,
    name VARCHAR(80) NOT NULL,
    short_description VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    author VARCHAR(255) NOT NULL,
    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ratings FLOAT NOT NULL,
    country VARCHAR(255) NOT NULL,
    categories VARCHAR(255)
);

