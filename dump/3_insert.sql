use food_camp;

-- Sample data for the food table
INSERT INTO food (name, short_description, description, author, ratings, country, categories)
VALUES
    ('Churros', 'Simple yet delicious', 'Detailed description for Churros + recepie', 'Alex K.', 4.5, 'Spain', 'Deserts'),
    ('Fish and Chips', 'Simple British breakfast', "Classic fish and chips are a British institution and a national dish that everyone can't help but love. You can buy them from one of the thousands of fish and chip shops all over the country, including the world-famous Harry Ramsden's, or you can make them at home. What Kind of Fish Works Best With This Batter? Use a thick white fish for this recipe; sustainable cod, haddock, or pollock are preferable. The batter includes both dark beer and sparkling water. The carbonation in the beer and sparkling water and the type of fish suggested ensures a light, mild, tender outcome, which is perfect for crispy fish and chips.", 'Alex K.', 3.8, 'United Kingdom', 'Everlasting Classics'),
    ('Pasta', 'Simple yet delicious', 'Detailed description for pasta', 'Alex K.', 4.2, 'Italy', 'Everlasting Classics;Main Dishes'),
    ('Pizza', 'Simple italian pizza', 'Detailed description for pizza', 'Alex K.', 5.0, 'Italy', 'Everlasting Classics;Main Dishes'),
    ('Jerk chicken/pork', 'Scotch bonnet peppers give jerk chicken its heat.', "Jamaica's favorite pepper is the Scotch bonnet, beloved not just for its spiciness but for its aroma, colors and flavor, too, says Mark Harvey, content creator and podcaster at Two On An Island, who was born in Spanish Town, Jamaica. For Jamaicans, the degree of spiciness starts at medium for children and goes up to purple hot, he says, explaining that the peppers come in green, orange, red and purple hues, growing increasingly spicy in that order. Scotch bonnets star in several of the island's iconic dishes, including escovitch fish, pepper pot soup and curry goat. But you might recognize them most from the ubiquitous jerk chicken and pork smoking roadside everywhere from Montego Bay to Boston Bay, where meat prepared with the peppery marinade is cooked the traditional way, atop coals from pimento tree wood (the tree's allspice berries are also used in the jerk marinade).", 'Alex K.', 3.1, 'Jamaica', 'Spicy Food');

-- Sample data for the users table (pass)
INSERT INTO users (email, username, password_hash, about_me, favourites)
VALUES
    ('admin@a', 'Alex K.', 'pbkdf2:sha256:260000$K279YUStOGpPdFie$38ef2e9e408e325b52e3d630229e7bf8ff55d992cbe353dcefc2f7024420ebf1', 'About me for Alex K.', '1;2;');