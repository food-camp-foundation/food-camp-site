import sqlalchemy as sa

from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.asyncio import AsyncSession
from dotenv import load_dotenv

from sqlalchemy_wrapper.select import SelectQuery
from sqlalchemy_wrapper.delete import DeleteQuery
from sqlalchemy_wrapper.statement import StatementQuery

from os import getenv, path
from sqlalchemy import MetaData


load_dotenv(path.join(path.abspath('env'), '.env'))
username = getenv('DB_USERNAME')
password = getenv('DB_PASSWORD')
host = getenv('DB_HOST')
port = getenv('DB_PORT')
db_name = getenv('DB_NAME')

connection_string = f'mysql+asyncmy://{username}:{password}@{host}:{port}/{db_name}'

meta = MetaData()
engine = create_async_engine(connection_string)
Session = AsyncSession(engine, expire_on_commit=False)


class DB:
    @classmethod
    def select(cls, fields=None):
        if fields is None:
            fields = ['*']
        return SelectQuery(cls, fields)

    @classmethod
    def update(cls):
        return StatementQuery(cls, sa.update(cls))

    @classmethod
    def insert(cls):
        return StatementQuery(cls, sa.insert(cls))

    @classmethod
    def delete(cls):
        return DeleteQuery(cls, sa.delete(cls))

    @staticmethod
    def before_save(data):
        pass

    @staticmethod
    def after_save(data):
        pass

    @classmethod
    def model_fields(cls):
        return list(filter(lambda x: not x.startswith('_'), dir(cls)))
