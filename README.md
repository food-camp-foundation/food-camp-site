# ***Food Camp*** - course project

## Description

- ### *Course project*: research features of object-relational mapping in web application based on `Python&Flask`.
- ### Create an extension/module based on `SQLAlchemy` to simplify the process.

## Used technologies

- `Python 3.10`
- `Flask` w/ Werkzeug Security
- `SQLAlchemy`
- SQLite / Postgres

## Startup

- Download / Clone this repo and start app using ***Python CLI***
  - Or start in any ***Python IDE***
- Download docker image on ***[dockerhub](https://www.youtube.com/watch?v=dQw4w9WgXcQ)***
- *[Direct link](http://localhost:5000)* after startup

## Author

- Krazhevskiy Aleksey
  - [github](https://github.com/alekseykrazhev)
  - [gitlab](https://gitlab.com/alekseykrazhev)
  - [dockerhub](https://hub.docker.com/u/alekseykrazhev)
