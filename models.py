import asyncio
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, LoginManager
from werkzeug.security import generate_password_hash, check_password_hash

from database import engine
from sqlalchemy.ext.declarative import declarative_base
from database import DB, meta

db = SQLAlchemy()
login = LoginManager()
Base = declarative_base(meta)


class FoodModel(Base, DB):
    __tablename__ = 'food'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), nullable=False)
    short_description = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    author = db.Column(db.String(255), nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    ratings = db.Column(db.Float, nullable=False)
    country = db.Column(db.String(255), nullable=False)
    categories = db.Column(db.Enum)

    def __init__(self, name, short_desc, desc, author, date, ratings, country, categories=None):
        super().__init__()
        self.name = name
        self.short_description = short_desc
        self.description = desc
        self.author = author
        self.date = date
        self.ratings = ratings
        self.country = country
        self.categories = categories

    @staticmethod
    async def get_all():
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            dishes = await FoodModel.select() \
                .all(conn)
            await engine.dispose()
        return dishes

    @staticmethod
    async def get_dish_by_id(food_id):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            dishes = await FoodModel.select() \
                .where(FoodModel.id == food_id) \
                .get(conn)
            await engine.dispose()
        return dishes

    @staticmethod
    async def get_dish_by_country(food_country):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            dishes = await FoodModel.select() \
                .where(FoodModel.country == food_country) \
                .all(conn)
            await engine.dispose()
        return dishes

    @staticmethod
    async def get_dish_by_category(food_category):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            dishes = await FoodModel.select() \
                .where(FoodModel.categories.like(f'%{food_category}%')) \
                .all(conn)
            await engine.dispose()
        return dishes

    @staticmethod
    async def all_by_username(user_id):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            dishes = await FoodModel.select() \
                .where(FoodModel.author == user_id) \
                .all(conn)
            await engine.dispose()
        return dishes

    @staticmethod
    async def create_dish(dish):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            _ = await FoodModel.insert().values(
                name=dish.name,
                short_description=dish.short_description,
                description=dish.description,
                author=dish.author,
                date=dish.date,
                ratings=dish.ratings,
                country=dish.country
            ).execute(conn)
            await engine.dispose()
        return True

    @staticmethod
    async def get_favourites(id_favourites):
        favourites = []
        for id_f in id_favourites:
            async with engine.begin() as conn:
                await conn.run_sync(meta.create_all)
                favourites.append(await FoodModel.select() \
                                  .where(FoodModel.id == id_f) \
                                  .get(conn))
                await engine.dispose()
        return favourites
    @staticmethod
    async def get_id(name):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            food = await FoodModel.select() \
                                .where(FoodModel.name == name) \
                                .get(conn)
            await engine.dispose()
        return food.id
    
    @staticmethod
    async def search_for_dish(name):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            dishes = await FoodModel.select() \
                .where(FoodModel.name.like(f'%{name}%')) \
                .all(conn)
            await engine.dispose()
        return dishes


class UserModel(UserMixin, Base, DB):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(80), unique=True)
    username = db.Column(db.String(100), unique=True)
    password_hash = db.Column(db.String())
    about_me = db.Column(db.String())
    favourites = db.Column(db.String())

    def __init__(self, email, username, password=None, about_me=None, favourites=None):
        super().__init__()
        self.email = email
        self.username = username
        self.password_hash = password
        self.about_me = about_me
        self.favourites = favourites

    def set_id(self, id):
        self.id = id

    def set_email(self, email):
        self.email = email

    def set_username(self, username):
        self.username = username

    def set_favourites(self, favourites):
        self.favourites = favourites

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    async def get_by_id(id):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            user = await UserModel.select() \
                .where(UserModel.id == id) \
                .get(conn)
            await engine.dispose()
        return user

    @staticmethod
    async def get_by_email(email):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            user = await UserModel.select() \
                .where(UserModel.email == email) \
                .get(conn)
            await engine.dispose()
        return user

    @staticmethod
    async def get_by_username(username):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            user = await UserModel.select() \
                .where(UserModel.username == username) \
                .get(conn)
            await engine.dispose()
        return user

    @staticmethod
    async def get_all_active():
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            users = await UserModel.select() \
                .where(UserModel.active.is_(True)) \
                .all(conn)
            await engine.dispose()
        return users

    @staticmethod
    async def create_user(user):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            _ = await UserModel.insert().values(
                email=user.email,
                username=user.username,
                password_hash=user.password_hash,
                about_me='',
                favourites=''
            ).execute(conn)
            await engine.dispose()
        return True

    @staticmethod
    async def update_user(user):
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            _ = await UserModel.update() \
                .values(
                email=user.email,
                username=user.username,
                password_hash=user.password_hash,
                about_me=user.about_me,
                favourites=user.favourites) \
                .where(UserModel.id == user.id) \
                .execute(conn)
            await engine.dispose()
        return True

    @staticmethod
    async def delete_user():
        async with engine.begin() as conn:
            await conn.run_sync(meta.create_all)
            _ = await UserModel.update().values(
                is_active=False
            ).execute(conn)
            await engine.dispose()
        return True


@login.user_loader
def load_user(user_id):
    """
    Given *user_id*, return the associated User object.

    :param unicode user_id: user_id (id) user to retrieve
    """
    user = asyncio.run(UserModel.get_by_id(user_id))
    # print('User object:', user)
    user_object = UserModel(user.email, user.username, user.password_hash)
    UserModel.get_by_id(user_id).close()
    user_object.set_id(user_id)
    return user_object
