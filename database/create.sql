CREATE SCHEMA IF NOT EXISTS `food-camp`;

CREATE TABLE IF NOT EXISTS `food` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(80) NOT NULL,
  `short_description` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `author` VARCHAR(255) NOT NULL,
  `date` DATE NOT NULL,
  `ratings` DOUBLE NOT NULL,
  `country` VARCHAR(255) NOT NULL,
  `categories` TEXT,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `username` VARCHAR(255) NOT NULL UNIQUE,
  `password_hash` VARCHAR(255) NOT NULL,
  `about_me` TEXT,
  `favourites` TEXT,
  PRIMARY KEY (`id`)
);


