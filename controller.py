import base64
import re
import dropbox

from os import getenv, path
from subprocess import check_output
from json import loads

from dotenv import load_dotenv
from PIL import Image
from io import BytesIO

from werkzeug.datastructures import FileStorage
from flask_caching import Cache


load_dotenv(path.join(path.abspath('env'), '.env'))

dropbox_app_key = getenv('DROPBOX_APP_KEY')
dropbox_app_secret = getenv('DROPBOX_APP_SECRET')
dropbox_refresh_token = getenv('DROPBOX_REFRESH_TOKEN')
dropbox_access_token = ''

cache = Cache(config={'CACHE_TYPE': 'SimpleCache'})


def crop_image(image):
    image = Image.open(image)
    cropped = image.crop((100, 100, 300, 300))
    cropped_io = BytesIO()
    cropped.save(cropped_io, format='JPEG')
    cropped_io.seek(0)
    cropped_file = FileStorage(cropped_io, filename='cropped.jpg', content_type='image/jpeg')
    return cropped_file


def get_access_token():
    global dropbox_access_token
    curl = f'curl https://api.dropbox.com/oauth2/token -d grant_type=refresh_token -d refresh_token=' \
           f'{dropbox_refresh_token} -u {dropbox_app_key}:{dropbox_app_secret}'
    output = check_output(curl, shell=True)
    response = loads(output.decode('utf-8'))
    dropbox_access_token = response['access_token']


def upload_image(image, name):
    global dropbox_access_token
    try:
        image_bytes = image.read()
    except AttributeError:
        image_bytes = image.tobytes()
    try:
        dbx = dropbox.Dropbox(dropbox_access_token)
    except (dropbox.exceptions.AuthError, dropbox.dropbox_client.BadInputException):
        get_access_token()
        dbx = dropbox.Dropbox(dropbox_access_token)

    dropbox_path = f'/Apps/food-camp/{name}.jpg'
    try:
        dbx.files_upload(image_bytes, dropbox_path)
    except dropbox.exceptions.ApiError:
        delete_image(dropbox_path)
    dbx.files_upload(image_bytes, dropbox_path)


def delete_image(image_path):
    deleted = False
    dbx = dropbox.Dropbox(dropbox_access_token)

    try:
        dbx.files_delete_v2(image_path)
        deleted = True
    except dropbox.exceptions.ApiError:
        pass
    return deleted


# @cache.memoize(timeout=720)
def get_image(name):
    image = None

    if path.exists(f'uploads/{name}.jpg'):
        image = Image.open(f'uploads/{name}.jpg')

        img_byte_array = BytesIO()
        image.save(img_byte_array, format='JPEG')
        img_byte_array = img_byte_array.getvalue()

        image = base64.b64encode(img_byte_array).decode('utf-8')
    else:
        try:
            dbx = dropbox.Dropbox(dropbox_access_token)
        except (dropbox.exceptions.AuthError, dropbox.dropbox_client.BadInputException):
            get_access_token()
            dbx = dropbox.Dropbox(dropbox_access_token)
        try:
            dropbox_path = f'/Apps/food-camp/{name}.jpg'
            _, response = dbx.files_download(dropbox_path)
            image_data = response.content
            image = base64.b64encode(image_data).decode('utf-8')
            image_bytes = BytesIO(image_data)
            image_save = Image.open(image_bytes)
            image_save = image_save.convert('RGB')
            image_save.save(f'uploads/{name}.jpg')
        except (dropbox.exceptions.HttpError, dropbox.exceptions.ApiError):
            pass
    return image


def validate_email(email):
    pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
    return re.match(pattern, email) is not None


def check_password_strength(password):
    pattern = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
    return re.match(pattern, password) is not None
