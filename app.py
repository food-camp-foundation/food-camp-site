import asyncio
import secrets
import pycountry
import logging

from datetime import datetime
from flask import Flask, request, render_template, redirect, url_for, flash, g, make_response
from werkzeug.exceptions import HTTPException
from werkzeug.utils import secure_filename

from flask_cors import CORS
from flask_login import login_user, login_required, logout_user, current_user
from flask_wtf import CSRFProtect
from flask_wtf.csrf import CSRFError

from controller import *
from database import connection_string
from models import FoodModel, UserModel, db, login

from sorting import selection_sort
from categories import get_categories

app = Flask(__name__)
secret_key = secrets.token_urlsafe(32)
app.secret_key = secret_key
csrf = CSRFProtect()

app.config['SQLALCHEMY_DATABASE_URI'] = connection_string
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
login.init_app(app)
cache.init_app(app)
csrf.init_app(app)

CORS(app)

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


@app.route('/')
def index():
    all_dishes = asyncio.run(FoodModel.get_all())
    FoodModel.get_all().close()
    all_dishes_list = []

    for dish in all_dishes:
        image_name = secure_filename(dish.name + dish.short_description)
        image = get_image(image_name)
        all_dishes_list.append({'id': dish.id, 'name': dish.name, 'ratings': dish.ratings, 'date':
            datetime.strftime(dish.date, "%d.%m.%Y"), 'image': image})

    sorted_dishes = selection_sort(all_dishes_list, 'ratings')

    length = len(sorted_dishes) - 20
    if length < 0:
        length = 0

    most_viewed = []
    for i in range(len(sorted_dishes) - 1, length - 1, -1):
        most_viewed.append(sorted_dishes[i])
    return render_template('index.html', dishes=most_viewed), 200


@app.route('/food', methods=['GET', 'POST'])
def food():
    if request.method == 'GET':
        all_dishes = asyncio.run(FoodModel.get_all())
        all_dishes_list = []

        for dish in all_dishes:
            image_name = secure_filename(dish.name + dish.short_description)
            image = get_image(image_name)
            all_dishes_list.append({'id': dish.id, 'name': dish.name, 'date': datetime.strftime(
                dish.date, "%d.%m.%Y"), 'image': image})
            # all_dishes_list.append({'id': dish.id, 'name': dish.name, 'date': datetime.strftime(
            #      dish.date, "%d.%m.%Y"), 'image': None})
        FoodModel.get_all().close()
        return render_template('food.html', dishes=all_dishes_list), 200

    if request.method == 'POST':
        return 'POST'


@app.route('/food/<int:food_id>')
def food_by_id(food_id):
    dish = asyncio.run(FoodModel.get_dish_by_id(food_id))
    dish_info = {'name': dish.name, 'short_description': dish.short_description,
                 'date': datetime.strftime(dish.date, "%d.%m.%Y"), 'description': dish.description,
                 'rate': dish.ratings, 'author': dish.author, 'id': food_id}
    FoodModel.get_dish_by_id(food_id).close()
    image_name = secure_filename(dish.name + dish.short_description)
    image = get_image(image_name)
    isFavourite = False
    try:
        user_id = g.user.id
        user_row = asyncio.run(UserModel.get_by_id(user_id))
        UserModel.get_by_id(g.user.id).close()
        favourites_list = user_row.favourites
        if str(food_id) in favourites_list:
            isFavourite = True
    except AttributeError:
        pass
    return render_template('food_by_id.html', dish_info=dish_info, image=image, isFavourite=isFavourite), 200


@app.route('/countries', methods=['GET', 'POST'])
def countries():
    if request.method == 'GET':
        return render_template('countries.html', countries=list(pycountry.countries)), 200
    if request.method == 'POST':
        country = request.form.get('selectedItem')
        dishes_row = asyncio.run(FoodModel.get_dish_by_country(country))
        FoodModel.get_dish_by_country(country).close()

        if not dishes_row:
            flash('No dishes from this country, yet :)')
            return redirect(url_for('countries'))

        # if len(dishes_row) == 1:
        #     return redirect(url_for('food_by_id', food_id=dishes_row[0].id))

        all_dishes_list = []
        for dish in dishes_row:
            image_name = secure_filename(dish.name + dish.short_description)
            image = get_image(image_name)
            all_dishes_list.append({'id': dish.id, 'name': dish.name, 'ratings': dish.ratings, 'date':
                datetime.strftime(dish.date, "%d.%m.%Y"), 'image': image})

        return render_template('index.html', dishes=all_dishes_list)


@app.route('/favourites', methods=['GET', 'POST'])
def favourites():
    user_row = asyncio.run(UserModel.get_by_id(g.user.id))
    UserModel.get_by_id(g.user.id).close()
    favourites_list = user_row.favourites

    if request.method == 'GET':
        if not favourites_list or len(favourites_list) == 0:
            return render_template('favourites.html'), 200

        favourites_list = favourites_list.split(';')
        favourites_list = list(filter(None, favourites_list))
        logger.info(f'Favourites list: {favourites_list}')
        fav_dishes = asyncio.run(FoodModel.get_favourites(favourites_list))
        all_dishes_list = []

        for dish in fav_dishes:
            image_name = secure_filename(dish.name + dish.short_description)
            image = get_image(image_name)
            all_dishes_list.append({'id': dish.id, 'name': dish.name, 'date': datetime.strftime(
                dish.date, "%d.%m.%Y"), 'image': image})
        FoodModel.get_favourites(favourites_list).close()

        return render_template('index.html', dishes=all_dishes_list)
    if request.method == 'POST':
        user_id = g.user.id
        request_data = request.json
        name = request_data.get('id')
        id = asyncio.run(FoodModel.get_id(name))
        FoodModel.get_id(name).close()
        if str(id) in favourites_list:
            favourites_list = favourites_list.replace(str(id) + ';', '')
            logger.info(f"Remove item from favourites: {str(id) + ';'}")
        else:
            favourites_list += f'{id};'
        logger.info(f'Favourites list: {favourites_list}')
        user = UserModel(user_row.email, user_row.username, user_row.password_hash, user_row.about_me)
        user.set_id(user_id)
        user.set_favourites(favourites_list)
        asyncio.run(UserModel.update_user(user))
        UserModel.update_user(user).close()
        return redirect(url_for('favourites'))


@app.route('/category', methods=['GET', 'POST'])
def category():
    if request.method == 'GET':
        return render_template('category.html', categories=get_categories()), 200
    if request.method == 'POST':
        selected_category = request.form.get('selectedItem')
        logger.info(f'Selected category: {selected_category}')
        dishes_row = asyncio.run(FoodModel.get_dish_by_category(selected_category))
        FoodModel.get_dish_by_category(selected_category).close()
        logger.info(f'Dishes row: {dishes_row}')
        if not dishes_row:
            flash('No dishes from this category, yet :)')
            return redirect(url_for('category'))

        # if len(dishes_row) == 1:
        #     return redirect(url_for('food_by_id', food_id=dishes_row[0].id))

        all_dishes_list = []
        for dish in dishes_row:
            image_name = secure_filename(dish.name + dish.short_description)
            image = get_image(image_name)
            all_dishes_list.append({'id': dish.id, 'name': dish.name, 'ratings': dish.ratings, 'date':
                datetime.strftime(dish.date, "%d.%m.%Y"), 'image': image})

        return render_template('index.html', dishes=all_dishes_list)


@app.route('/about')
def about():
    return render_template('about.html'), 200


@app.route('/faq')
def faq():
    return render_template('faq.html'), 200


@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    if request.method == 'POST':
        user_id = g.user.id
        image = request.files['image']
        username = request.form['username']
        about_me = request.form['about_me']

        user_row = asyncio.run(UserModel.get_by_id(user_id))
        user = UserModel(user_row.email, user_row.username, user_row.password_hash)
        user.set_id(user_id)
        UserModel.get_by_id(user_id).close()

        if image is not None:
            upload_image(image, user_id)

        if username != '':
            user.set_username(username)

        if about_me != '':
            user.about_me = about_me

        asyncio.run(UserModel.update_user(user))
        UserModel.update_user(user).close()

        return redirect(url_for('profile'))

    user_id = g.user.id
    profile_image = get_image(user_id)

    user_row = asyncio.run(UserModel.get_by_id(user_id))
    UserModel.get_by_id(user_id).close()
    posts_row = asyncio.run(FoodModel.all_by_username(user_row.username))
    FoodModel.all_by_username(user_row.username).close()

    dishes = []

    for post in posts_row:
        image_name = secure_filename(post.name + post.short_description)
        image = get_image(image_name)
        dishes.append({'id': post.id, 'name': post.name, 'ratings': post.ratings, 'date':
            datetime.strftime(post.date, "%d.%m.%Y"), 'image': image})

    return render_template('profile.html', image=profile_image, user=user_row, dishes=dishes), 200


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        remember = request.form.get('remember')
        user = asyncio.run(UserModel.get_by_email(username))
        if not (not user or not UserModel.check_password(user, password)):
            user_object = UserModel(user.email, user.username, user.password_hash)
            user_object.set_id(user.id)
            UserModel.get_by_email(username).close()
            if remember == 'on':
                login_user(user_object, remember=True)
            else:
                login_user(user_object)
            return redirect(url_for('index'))
        else:
            flash('Invalid username or password')
            UserModel.get_by_email(username).close()
    return render_template('login.html'), 200


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        confirm_password = request.form['confirm_password']
        if password == '' or email == '' or username == '':
            flash('Please, input all the fields')
            return redirect(url_for('signup'))
        # if not check_password_strength(password):
        #     flash("""<strong>Password is not secure:</strong><br> 
        #     - Minimum length of 8 characters<br>
        #     - At least one uppercase letter<br>
        #     - At least one lowercase letter<br>
        #     - At least one digit<br>
        #     - At least one special character (such as !@#$%^&*)<br>""")
        #     return redirect(url_for('signup'))
        if password != confirm_password:
            flash('Passwords do not match')
            return redirect(url_for('signup'))

        user_email = asyncio.run(UserModel.get_by_email(email))
        if user_email is not None:
            flash('Email is already taken')
            return redirect(url_for('signup'))
        UserModel.get_by_email(email).close()
        user_username = asyncio.run(UserModel.get_by_username(username))
        if user_username is not None:
            flash('Username is already taken')
            return redirect(url_for('signup'))
        UserModel.get_by_username(username).close()
        # if not validate_email(email):
        #     flash('Invalid email address')
        #     return redirect(url_for('signup'))
        user = UserModel(email, username)
        user.set_password(password)
        asyncio.run(UserModel.create_user(user))
        UserModel.create_user(user).close()
        user = asyncio.run(UserModel.get_by_email(email))
        user_object = UserModel(user.email, user.username, user.password_hash)
        user_object.set_id(user.id)
        UserModel.get_by_email(username).close()
        print('User object: ', user)
        logger.info(f'User object: {user_object}')
        logger.info(f'User object: {user_object.email}')
        logger.info(f'User object: {user_object.id}')
        res = login_user(user_object)
        if res:
            logger.info('Successfully created user!')
        else:
            logger.info('Could not log new user in, check...')
        return redirect(url_for('index'))
    return render_template('signup.html'), 200


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    if request.method == 'POST':
        name = request.form['name']
        short_description = request.form['short_description']
        image = request.files['image1']
        description = request.form['description']
        country = request.form.get('searchInput')
        category = request.form.get('searchInput1')
        print(country, category)
        user_row = asyncio.run(UserModel.get_by_id(g.user.id))
        new_dish = FoodModel(name, short_description, description, user_row.username, datetime.now(), 0.0, country)
        UserModel.get_by_id(g.user.id).close()
        asyncio.run(FoodModel.create_dish(new_dish))
        FoodModel.create_dish(new_dish).close()
        image_name = secure_filename(name + short_description)
        upload_image(image, image_name)
        return redirect(url_for('index'))
    return render_template('create.html', countries=pycountry.countries, categories=get_categories()), 200


@app.route('/search', methods=['GET', 'POST'])
def search():
    query = request.form.get('searchInput')
    # logger.info(f'Search query: {query}')
    
    dishes_row = asyncio.run(FoodModel.search_for_dish(query))
    FoodModel.search_for_dish(query).close()
    # logger.info(f'Search result: {dishes_row}')

    all_dishes_list = []
    for dish in dishes_row:
        image_name = secure_filename(dish.name + dish.short_description)
        image = get_image(image_name)
        all_dishes_list.append({'id': dish.id, 'name': dish.name, 'ratings': dish.ratings, 'date':
            datetime.strftime(dish.date, "%d.%m.%Y"), 'image': image})

    return render_template('index.html', dishes=all_dishes_list)


@app.before_request
def before_request():
    g.user = current_user


@app.after_request
def add_header(response):
    response.headers['X-Frame-Options'] = 'SAMEORIGIN'
    response.headers['Content-Security-Policy'] = \
        "default-src * 'unsafe-inline'; style-src * 'unsafe-inline'; img-src data: *;"
    return response


@app.errorhandler(HTTPException)
def handle_exception(e):
    return render_template('error.html', error_message=e.description, status_code=e.code)


@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    return render_template('csrf_error.html', reason=e.description), 400


if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
